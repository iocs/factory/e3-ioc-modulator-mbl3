require essioc
require modbus
require modulator

epicsEnvSet(SYS, "MBL")
epicsEnvSet(SUB, "030RFC")
epicsEnvSet(ID, "010")
epicsEnvSet(MOD_IP, "mbl-030-modulator.tn.esss.lu.se")
epicsEnvSet(PSS_PV, "Spk-070Row:CnPw-U-017:MBL03_Sys1_ToLPS")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

dbLoadRecords("$(E3_CMD_TOP)/pss_bypass.db","P=$(SYS)-$(SUB):RFS-Mod-$(ID):")

pvlistFromInfo("ARCHIVE_THIS","$(IOCNAME):ArchiverList")

